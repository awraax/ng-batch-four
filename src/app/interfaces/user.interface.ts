export interface User {
  id: number,
  username: string,
  email: string,
  noTelp: string,
  password: string,
  role: string,
  image: string
}

export interface ResponseUploadPhoto{
  image: string
}
